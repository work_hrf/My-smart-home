目录结构说明
1      server
         服务器端程序(主要在ubuntu10.10上测试, linux 平台编译，运行都是没问题的)
         怎么编译：
         cd  src
          make    
         怎么运行：
          ./redis-server --loglevel verbose    (--loglevel verbose 表示带调试信息，默认端口为6379)
2
      android_client  
       为andriod平台的客户端，用eclipse打开即可。

3
     debug_script
     为mysql数据库初始话，测试用例的一些脚本

4
     www
     http服务器用的代码，简单的web控制 

     有任何问题可以联系,或者要添加测试用户等  :   liyongming1982@163.com
     
5   更详细的说明，麻烦移步：
    http://git.oschina.net/xmeter/My-smart-home/wikis/home
    
6
    希望有新的小伙伴加入，随时联系： liyongming1982@163.com